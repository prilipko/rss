package nl.tt.rss.dao;

import org.springframework.data.repository.CrudRepository;

import nl.tt.rss.domain.Feed;

public interface FeedRepository extends CrudRepository<Feed, Long> {

}
