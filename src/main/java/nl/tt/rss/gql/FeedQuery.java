package nl.tt.rss.gql;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import nl.tt.rss.dao.FeedRepository;
import nl.tt.rss.domain.Feed;

@Component
@RequiredArgsConstructor
public class FeedQuery implements GraphQLQueryResolver {

    private final FeedRepository feedRepo;


    public Iterable<Feed> feeds() {
        return feedRepo.findAll();
    }

}
