package nl.tt.rss.service;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.rometools.rome.feed.synd.SyndEnclosure;
import com.rometools.rome.feed.synd.SyndEntry;

import nl.tt.rss.domain.FeedItem;

@Component
public class FeedItemMapper {

    public FeedItem toDomain(final SyndEntry entry, final Long feedId) {
        return FeedItem.builder()
                .feedId(feedId)
                .description(entry.getDescription().getValue())
                .uri(entry.getUri())
                .link(entry.getLink())
                .publicationDate(entry.getPublishedDate().toInstant())
                .title(entry.getTitle())
                .imageUrl(getImageUrl(entry).orElse(null))
                .build();
    }

    private Optional<String> getImageUrl(final SyndEntry entry) {
        return entry.getEnclosures()
                .stream()
                .filter(enclosure -> enclosure.getType().contains("image"))
                .findAny()
                .map(SyndEnclosure::getUrl);
    }
}
