package nl.tt.rss.service;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "rss")
public class RssProperties {

    private long maxItemsPerFeed = Long.MAX_VALUE;
}
