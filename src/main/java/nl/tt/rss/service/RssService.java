package nl.tt.rss.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.tt.rss.dao.FeedRepository;
import nl.tt.rss.domain.FeedItem;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class RssService {

    @NonNull
    private final RssProperties properties;
    @NonNull
    private final FeedRepository feedRepo;
    @NonNull
    private final FeedItemMapper feedItemMapper;

    public void updateRepositories() {
        feedRepo.findAll().forEach(feed -> {
            SyndFeedInput input = new SyndFeedInput();

            log.info("Work on feed: {}", feed.getUrl());
            try {
                SyndFeed syndFeed = input.build(new XmlReader(new URL(feed.getUrl())));

                var oldItems = feed.getItems();
                var receivedItems = syndFeed.getEntries()
                        .stream()
                        .map(entry -> feedItemMapper.toDomain(entry, feed.getId()))
                        .collect(Collectors.toList());

                receivedItems.forEach(this::loadImage);

                var newItems = Stream.concat(oldItems.stream(), receivedItems.stream())
                        .collect(Collectors.toMap(FeedItem::getUri, feedItem -> feedItem, this::mergeItem))
                        .values()
                        .stream()
                        .sorted(Comparator.comparing(FeedItem::getPublicationDate))
                        .limit(properties.getMaxItemsPerFeed())
                        .collect(Collectors.toSet());

                feed.getItems().clear();
                feed.getItems().addAll(newItems);

                feedRepo.save(feed);
            } catch (FeedException | IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void loadImage(final FeedItem item) {
        if (item.getImageUrl() == null) {
            return;
        }
        try (final InputStream is = new URL(item.getImageUrl()).openStream()) {
            final byte[] image = is.readAllBytes();
            item.setData(image);
        } catch (IOException e) {
            log.info("Exception during loading file", e);
        }
    }

    private FeedItem mergeItem(final FeedItem item1, final FeedItem item2) {
        final FeedItem to;
        final FeedItem from;
        if (areSame(item1, item2)) {
            return item1.getId() != null ? item1 : item2;
        } else if (item1.getId() != null) {
            to = item1;
            from = item2;
        } else {
            to = item2;
            from = item1;
        }
        to.setTitle(from.getTitle());
        to.setImageUrl(from.getImageUrl());
        to.setLink(from.getLink());
        to.setPublicationDate(from.getPublicationDate());
        to.setDescription(from.getDescription());
        to.setData(from.getData());
        return to;
    }

    private boolean areSame(final FeedItem item1, final FeedItem item2) {
        return Objects.equals(item1.getLink(), item2.getLink())
                && Objects.equals(item1.getTitle(), item2.getTitle())
                && Objects.equals(item1.getDescription(), item2.getDescription())
                && Objects.equals(item1.getPublicationDate(), item2.getPublicationDate())
                && Objects.equals(item1.getImageUrl(), item2.getImageUrl())
                && Arrays.equals(item1.getData(), item2.getData());
    }
}
