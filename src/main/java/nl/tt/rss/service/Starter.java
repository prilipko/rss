package nl.tt.rss.service;

import javax.annotation.PostConstruct;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class Starter {

    private final RssService rssService;

    @Scheduled(cron = "0 */5 * * * *")
    public void regularWork() {
        rssService.updateRepositories();
    }

    @PostConstruct
    public void onStartup() {
        rssService.updateRepositories();
    }
}
