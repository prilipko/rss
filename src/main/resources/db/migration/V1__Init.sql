CREATE TABLE feed
(
    id  BIGINT AUTO_INCREMENT PRIMARY KEY,
    url VARCHAR(250) NOT NULL
);

CREATE TABLE feed_item
(
    id               BIGINT AUTO_INCREMENT PRIMARY KEY,
    uri              VARCHAR(250) NOT NULL,
    link             VARCHAR(250),
    title            VARCHAR(250),
    description      VARCHAR,
    publication_date TIMESTAMP WITHOUT TIME ZONE,
    image_url        VARCHAR(250),
    feed_id          BIGINT,
    data             BLOB
);

INSERT INTO feed
VALUES (null, 'http://feeds.nos.nl/nosjournaal?format=xml')
